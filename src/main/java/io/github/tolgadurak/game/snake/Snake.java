package io.github.tolgadurak.game.snake;

import java.util.ArrayList;
import java.util.List;

public class Snake {
	private int direction = 8;
	private int size;
	private List<Integer> xCoord = new ArrayList<>();
	private List<Integer> yCoord = new ArrayList<>();

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public List<Integer> getxCoord() {
		return xCoord;
	}

	public void setxCoord(List<Integer> xCoord) {
		this.xCoord = xCoord;
	}

	public List<Integer> getyCoord() {
		return yCoord;
	}

	public void setyCoord(List<Integer> yCoord) {
		this.yCoord = yCoord;
	}
}
