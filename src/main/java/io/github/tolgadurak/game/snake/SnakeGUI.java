package io.github.tolgadurak.game.snake;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SnakeGUI {

	private static final Logger logger = LogManager.getLogger(SnakeGUI.class);
	private JFrame mainFrame = new JFrame();
	MouseEvent pressed;

	public SnakeGUI() throws IOException {
		mainFrame.setLocation(400, 200);
		mainFrame.setUndecorated(true);
		mainFrame.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				mainFrame.setShape(
						new RoundRectangle2D.Double(0, 0, mainFrame.getWidth(), mainFrame.getHeight(), 20, 20));
			}
		});

		mainFrame.addMouseMotionListener(new MouseMotionListener() {

			@Override
			public void mouseMoved(MouseEvent e) {
				// not considered yet
			}

			@Override
			public void mouseDragged(MouseEvent e) {
				mainFrame.setLocation(mainFrame.getLocationOnScreen().x - (pressed.getX() - e.getX()),
						mainFrame.getLocationOnScreen().y - (pressed.getY() - e.getY()));
			}
		});
		mainFrame.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				pressed = e;
			}
		});
		try {
			snakeSettingsGUI = new SnakeSettings();
		} catch (IOException ex) {
			logger.error(ex.getMessage(), ex);
		}
		snakeSettingsGUI.getFrame().setLocation(500, 300);
		handler = new DirectionHandler();
		initComponents();
		InputStream is = getClass().getResourceAsStream("image/snake-icon.png");
		bufImgIcon = ImageIO.read(is);
		setProgramIcon();
		setMainLogo();
		setBackgroundImage();

		startGameButton.setContentAreaFilled(false);
		startGameButton.setFocusPainted(false);
		startGameButton.setBorderPainted(false);

		settingsButton.setContentAreaFilled(false);
		settingsButton.setFocusPainted(false);
		settingsButton.setBorderPainted(false);

		exitButton.setContentAreaFilled(false);
		exitButton.setFocusPainted(false);
		exitButton.setBorderPainted(false);

	}

	private void setBackgroundImage() throws IOException {
		InputStream is = getClass().getResourceAsStream("image/snake_texture.png");
		BufferedImage bufImg = ImageIO.read(is);
		backgroundLabel.setIcon(new ImageIcon(bufImg));

	}

	private void setProgramIcon() {
		mainFrame.setIconImage(bufImgIcon);
	}

	private void setMainLogo() throws IOException {
		InputStream is = getClass().getResourceAsStream("image/snakeLogo.png");
		BufferedImage bufImg = ImageIO.read(is);
		logoLabel.setIcon(new ImageIcon(bufImg));
		logoLabel.repaint();
		logoLabel.revalidate();
	}

	private void initComponents() {
		javax.swing.JMenu fileMenu = new javax.swing.JMenu();
		javax.swing.JMenuItem scoresMenuItem = new javax.swing.JMenuItem();
		javax.swing.JMenuItem exitMenuItem = new javax.swing.JMenuItem();
		javax.swing.JMenu helpMenu = new javax.swing.JMenu();
		javax.swing.JMenuItem aboutMenuItem = new javax.swing.JMenuItem();
		java.awt.Font fontForButtons = new java.awt.Font("Tahoma", 1, 25);
		mainFrame.setFocusable(true);
		mainFrame.requestFocus();
		mainPanel = new javax.swing.JPanel();
		logoLabel = new javax.swing.JLabel();
		exitButton = new javax.swing.JButton();
		backgroundLabel = new javax.swing.JLabel();
		buttonsPanel = new javax.swing.JPanel();
		startGameButton = new JButton();
		settingsButton = new JButton();
		snakeAppMenuBar = new javax.swing.JMenuBar();
		mainFrame.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		mainFrame.setTitle("Snake Game v.1.0.0");
		mainPanel.setOpaque(false);
		logoLabel.setOpaque(false);
		backgroundLabel.setText(null);
		buttonsPanel.setOpaque(false);
		startGameButton.setFont(fontForButtons);
		startGameButton.setText("Start");
		startGameButton.setPreferredSize(new java.awt.Dimension(131, 37));
		startGameButton.addActionListener((java.awt.event.ActionEvent evt) -> {
			try {
				startGameButtonActionPerformed();
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
		});
		startGameButton.setOpaque(true);
		settingsButton.setFont(fontForButtons);
		settingsButton.setText("Settings");
		settingsButton.addActionListener((java.awt.event.ActionEvent evt) -> {
			snakeSettingsGUI.getFrame().setVisible(true);
			snakeSettingsGUI.getSaveButton().setEnabled(false);
		});
		settingsButton.setOpaque(true);
		fileMenu.setText("File");
		scoresMenuItem.setText("Highscores");
		fileMenu.add(scoresMenuItem);
		exitButton.setFont(fontForButtons);
		exitButton.setText("Exit");
		exitButton.addActionListener((java.awt.event.ActionEvent evt) -> exitButtonActionPerformed());
		exitMenuItem.setText("Exit");
		exitMenuItem.addActionListener((java.awt.event.ActionEvent evt) -> exitMenuItemActionPerformed());
		fileMenu.add(exitMenuItem);
		snakeAppMenuBar.add(fileMenu);
		helpMenu.setText("Help");
		helpMenu.addActionListener((java.awt.event.ActionEvent evt) -> helpMenuActionPerformed());
		aboutMenuItem.setText("About");
		aboutMenuItem.addActionListener((java.awt.event.ActionEvent evt) -> aboutMenuItemActionPerformed());
		helpMenu.add(aboutMenuItem);
		snakeAppMenuBar.add(helpMenu);
		setCustomLayout();
	}

	private void exitMenuItemActionPerformed() {
		terminateProgram();
	}

	private void terminateProgram() {
		logger.info("Program is terminating..");
		System.exit(1);
	}

	private void helpMenuActionPerformed() {
		// not considered yet
	}

	private void aboutMenuItemActionPerformed() {
		try {
			InputStream is = getClass().getResourceAsStream("/image/snake-icon-about.png");
			BufferedImage bufImg = ImageIO.read(is);
			ImageIcon image = new ImageIcon(bufImg);
			JLabel label = new JLabel("<html>" + "<p  style=\"text-align:center;\">" + "Snake Game version 1.0.0"
					+ " <br> " + "(This is non-commercial software)" + "<br>" + "Author: Tolga Durak" + "<br>"
					+ "Contact us:\n" + "tolgadurak.dev@gmail.com" + "<p>" + "</html>", image, JLabel.CENTER);
			label.setHorizontalTextPosition(JLabel.CENTER);
			label.setVerticalTextPosition(JLabel.BOTTOM);
			JOptionPane optionPane = new JOptionPane(label);
			JDialog dialog = optionPane.createDialog("");
			dialog.setTitle("About Snake Game");
			dialog.setIconImage(bufImgIcon);
			dialog.setModal(true);
			dialog.setVisible(true);
		} catch (IOException ex) {
			logger.error(ex.getMessage(), ex);
		}
	}

	private void exitButtonActionPerformed() {
		terminateProgram();
	}

	private void startGameButtonActionPerformed() throws IOException {
		mainFrame.addKeyListener(handler);
		Properties.setScore(0);
		gameController = new GameController();
		gameController.getSnakeGraphicsEngine().getMainTimer().start();
		gameController.getSnake().setxCoord(new ArrayList<Integer>());
		gameController.getSnake().setyCoord(new ArrayList<Integer>());
		gameController.getSnake().getxCoord().add(Properties.getInitX1());
		gameController.getSnake().getxCoord().add(Properties.getInitX2());
		gameController.getSnake().getyCoord().add(Properties.getInitY1());
		gameController.getSnake().getyCoord().add(Properties.getInitY2());
		try {
			gameController.getSnake().setDirection(8);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		mainFrame.remove(buttonsPanel);
		mainFrame.remove(mainPanel);
		mainFrame.remove(backgroundLabel);
		mainFrame.setResizable(true);
		mainFrame.setLayout(new BorderLayout());
		graphicsEngine = gameController.getSnakeGraphicsEngine();
		mainFrame.add(graphicsEngine, BorderLayout.CENTER);
		mainFrame.repaint();
		mainFrame.revalidate();
	}

	public class DirectionHandler implements KeyListener {
		@Override
		public void keyTyped(KeyEvent e) {
			// Not considered yet
		}

		@Override
		public void keyReleased(KeyEvent e) {
			// Not implemented yet
		}

		@Override
		public void keyPressed(KeyEvent e) {
			int keyCode = e.getKeyCode();
			Snake snake = gameController.getSnake();
			boolean snakeDirectionLeftOrRight = snake.getDirection() != 8 && snake.getDirection() != 2;
			boolean snakeDirectionUpOrDown = snake.getDirection() != 4 && snake.getDirection() != 6;
			if (keyCode == KeyEvent.VK_UP || keyCode == KeyEvent.VK_W) {
				changeDirectionUnderCondition(8, snakeDirectionLeftOrRight);
			} else if (keyCode == KeyEvent.VK_LEFT || keyCode == KeyEvent.VK_A) {
				changeDirectionUnderCondition(4, snakeDirectionUpOrDown);
			} else if (keyCode == KeyEvent.VK_RIGHT || keyCode == KeyEvent.VK_D) {
				changeDirectionUnderCondition(6, snakeDirectionUpOrDown);
			} else if (keyCode == KeyEvent.VK_DOWN || keyCode == KeyEvent.VK_S) {
				changeDirectionUnderCondition(2, snakeDirectionLeftOrRight);
			}
		}

		private void changeDirectionUnderCondition(int direction, boolean condition) {
			if (condition) {
				gameController.getSnake().setDirection(direction);
				addXCoordToBendSnake();
				addYCoordToBendSnake();
			}
		}

		private void addXCoordToBendSnake() {
			gameController.getSnake().getxCoord()
					.add(gameController.getSnake().getxCoord().get((gameController.getSnake().getxCoord().size() - 1)));
		}

		private void addYCoordToBendSnake() {
			gameController.getSnake().getyCoord()
					.add(gameController.getSnake().getyCoord().get(gameController.getSnake().getyCoord().size() - 1));
		}
	}

	public void setCustomLayout() {
		mainFrame.setPreferredSize(new Dimension(580, 300));
		mainFrame.setResizable(false);

		javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
		mainPanel.setLayout(mainPanelLayout);
		mainPanelLayout
				.setHorizontalGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(mainPanelLayout.createSequentialGroup().addContainerGap()
								.addComponent(logoLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 550, Short.MAX_VALUE)
								.addContainerGap()));
		mainPanelLayout.setVerticalGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(logoLabel, javax.swing.GroupLayout.Alignment.TRAILING,
						javax.swing.GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE));

		javax.swing.GroupLayout buttonsPanelLayout = new javax.swing.GroupLayout(buttonsPanel);
		buttonsPanel.setLayout(buttonsPanelLayout);
		buttonsPanelLayout
				.setHorizontalGroup(buttonsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
								buttonsPanelLayout.createSequentialGroup().addGap(85, 85, 85)
										.addComponent(startGameButton, javax.swing.GroupLayout.PREFERRED_SIZE, 155,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(settingsButton, javax.swing.GroupLayout.PREFERRED_SIZE, 155,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(exitButton, javax.swing.GroupLayout.PREFERRED_SIZE, 155,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addContainerGap(40, Short.MAX_VALUE)));
		buttonsPanelLayout.setVerticalGroup(buttonsPanelLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(buttonsPanelLayout.createSequentialGroup().addGroup(buttonsPanelLayout
						.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(buttonsPanelLayout.createSequentialGroup()
								.addGroup(buttonsPanelLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
										.addComponent(settingsButton, javax.swing.GroupLayout.PREFERRED_SIZE, 70,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(startGameButton, javax.swing.GroupLayout.PREFERRED_SIZE, 70,
												javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGap(0, 0, Short.MAX_VALUE))
						.addComponent(exitButton, javax.swing.GroupLayout.Alignment.TRAILING,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE))
						.addContainerGap()));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(mainFrame.getContentPane());
		mainFrame.getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(buttonsPanel, javax.swing.GroupLayout.PREFERRED_SIZE,
						javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup()
								.addComponent(mainPanel, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addContainerGap()))
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(
						backgroundLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 580,
						javax.swing.GroupLayout.PREFERRED_SIZE)));
		layout.setVerticalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						javax.swing.GroupLayout.Alignment.TRAILING,
						layout.createSequentialGroup().addGap(184, 184, 184)
								.addComponent(buttonsPanel, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addGap(49, 49, 49))
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup()
								.addComponent(mainPanel, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addGap(134, 134, 134)))
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(
						backgroundLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 314,
						javax.swing.GroupLayout.PREFERRED_SIZE)));
		mainFrame.pack();

	}

	private javax.swing.JLabel logoLabel;
	private javax.swing.JPanel mainPanel;
	private javax.swing.JPanel buttonsPanel;
	private JButton settingsButton;
	private javax.swing.JMenuBar snakeAppMenuBar;
	private JButton startGameButton;
	private SnakeSettings snakeSettingsGUI;
	private BufferedImage bufImgIcon;
	private GameController gameController;
	private DirectionHandler handler;
	private SnakeGraphicsEngine graphicsEngine;
	private javax.swing.JLabel backgroundLabel;
	private javax.swing.JButton exitButton;

	public SnakeGraphicsEngine getSnakeGraphicsEngine() {
		return graphicsEngine;
	}

	public javax.swing.JMenuBar getSnakeAppMenuBar() {
		return snakeAppMenuBar;
	}

	public JFrame getMainFrame() {
		return mainFrame;
	}

}
