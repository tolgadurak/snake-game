package io.github.tolgadurak.game.snake;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineEvent.Type;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.Timer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GameController {

	private Snake snake;
	private SnakeGraphicsEngine graphicsEngine;
	private boolean isStarted;
	private int starterCount;
	private BufferedImage bufImgIcon;
	private Timer mainTimer;
	private MainTimerHandler mainTimerHandler;
	private FoodHandler foodHandler;
	private boolean isFoodTimerRunning;
	private boolean isEaten;
	private Timer foodTimer;
	private int snakeX;
	private int snakeY;
	private InputStream imageIconStream;
	private static final Logger logger = LogManager.getLogger(GameController.class);

	public GameController() throws IOException {

		snake = new Snake();
		graphicsEngine = new SnakeGraphicsEngine(snake);
		mainTimerHandler = new MainTimerHandler();
		foodHandler = new FoodHandler();
		mainTimer = new Timer(Properties.getSpeedOfGame(), mainTimerHandler);
		foodTimer = new Timer(1, foodHandler);
		mainTimer.start();
		foodTimer.start();
		imageIconStream = getClass().getResourceAsStream("image/snake-icon.png");
		bufImgIcon = ImageIO.read(imageIconStream);

	}

	public void freeSubThreads() {
		freeMainTimerOfGraphicsEngine();
		freeFoodTimer();
		freeMainTimer();
	}

	public void freeResources() {
		try {
			imageIconStream.close();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}

	private void freeMainTimer() {
		mainTimer.stop();
	}

	private void freeFoodTimer() {
		foodTimer.stop();
	}

	private void freeMainTimerOfGraphicsEngine() {
		graphicsEngine.freeMainTimer();
	}

	public Timer getFoodTimer() {
		return foodTimer;
	}

	public void setFoodTimer(Timer foodTimer) {
		this.foodTimer = foodTimer;
	}

	private class MainTimerHandler implements ActionListener {

		public void actionPerformed(ActionEvent actionEvent) {
			snakeX = snake.getxCoord().get(snake.getxCoord().size() - 1);
			snakeY = snake.getyCoord().get(snake.getyCoord().size() - 1);
			if (isStarted && starterCount == 0) {
				starterCount++;
			}
			controlBounds();
			if (!isSnakeHitItself()) {
				publishSnakeHitIlselfEvent();
			}
			if (snake.getxCoord().size() > 2) {
				yesCurveCond();
			} else {
				noCurveCond();
			}
			if (!isFoodTimerRunning)
				isFoodTimerRunning = true;

		}

		private void publishSnakeHitIlselfEvent() {
			playSnakeEatenSound();
			ImageIcon imgIcon = new ImageIcon(bufImgIcon);
			int choice = JOptionPane.showConfirmDialog(null,
					Properties.FAILURE_MESSAGE + Properties.BACKSLASH_N_YOUR_SCORE_IS + Properties.getScore(),
					Properties.FAILURE_TITLE, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, imgIcon);
			Properties.setScore(0);
			if (choice == JOptionPane.YES_NO_OPTION) {
				resetGame();
			} else {
				exitGame();
			}
		}

		private void playSnakeEatenSound() {
			try {
				Properties.getSnakeEaten().start();
				Properties.getSnakeEaten().addLineListener((LineEvent event) -> {
					if (Type.STOP.equals(event.getType())) {
						Properties.getSnakeEaten().setMicrosecondPosition(0);
						Properties.getSnakeEaten().setFramePosition(0);
					}
				});

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}

		private void noCurveCond() {
			switch (snake.getDirection()) {
			case 2:
				snake.getyCoord().set(1, snake.getyCoord().get(1) + 2);
				snake.getyCoord().set(0, snake.getyCoord().get(0) + 2);
				break;
			case 4:
				snake.getxCoord().set(1, snake.getxCoord().get(1) - 2);
				snake.getxCoord().set(0, snake.getxCoord().get(0) - 2);
				break;
			case 6:
				snake.getxCoord().set(1, snake.getxCoord().get(1) + 2);
				snake.getxCoord().set(0, snake.getxCoord().get(0) + 2);
				break;
			case 8:
				snake.getyCoord().set(1, snake.getyCoord().get(1) - 2);
				snake.getyCoord().set(0, snake.getyCoord().get(0) - 2);
				break;

			default:
				break;
			}
		}

		private void yesCurveCond() {
			switch (snake.getDirection()) {
			case 2:
				if (controlAndProcessCurves())
					snake.getyCoord().set(snake.getyCoord().size() - 1,
							snake.getyCoord().get(snake.getyCoord().size() - 1) + 2);
				break;
			case 4:
				if (controlAndProcessCurves())
					snake.getxCoord().set(snake.getxCoord().size() - 1,
							snake.getxCoord().get(snake.getxCoord().size() - 1) - 2);
				break;
			case 6:
				if (controlAndProcessCurves())
					snake.getxCoord().set(snake.getxCoord().size() - 1,
							snake.getxCoord().get(snake.getxCoord().size() - 1) + 2);
				break;
			case 8:
				if (controlAndProcessCurves())
					snake.getyCoord().set(snake.getyCoord().size() - 1,
							snake.getyCoord().get(snake.getyCoord().size() - 1) - 2);
				break;

			default:
				break;
			}
		}

		private boolean controlAndProcessCurves() {
			if (snake.getyCoord().get(0).equals(snake.getyCoord().get(1))) {
				if (snake.getxCoord().get(0) > snake.getxCoord().get(1)) {
					snake.getxCoord().set(0, snake.getxCoord().get(0) - 2);
				} else if (snake.getxCoord().get(0) < snake.getxCoord().get(1)) {
					snake.getxCoord().set(0, snake.getxCoord().get(0) + 2);
				} else {
					snake.getxCoord().remove(0);
					snake.getyCoord().remove(0);
					return false;
				}
			} else {
				if (snake.getyCoord().get(0) > snake.getyCoord().get(1)) {
					snake.getyCoord().set(0, snake.getyCoord().get(0) - 2);
				} else if (snake.getyCoord().get(0) < snake.getyCoord().get(1)) {
					snake.getyCoord().set(0, snake.getyCoord().get(0) + 2);
				} else {
					snake.getxCoord().remove(0);
					snake.getyCoord().remove(0);
					return false;
				}
			}
			return true;
		}

		private boolean isSnakeHitItself() {
			int headOfSnakeX = snake.getxCoord().get(snake.getxCoord().size() - 1);
			int headOfSnakeY = snake.getyCoord().get(snake.getyCoord().size() - 1);
			int snakeBendSize = snake.getxCoord().size();

			if (snakeBendSize >= 5) {
				switch (snake.getDirection()) {
				case 4:
					return checkIfSnakeIsAliveWhileGoingOnXAxis(snakeBendSize, headOfSnakeX, headOfSnakeY);

				case 2:
					return checkIfSnakeIsAliveWhileGoingOnYAxis(snakeBendSize, headOfSnakeX, headOfSnakeY);

				case 6:
					return checkIfSnakeIsAliveWhileGoingOnXAxis(snakeBendSize, headOfSnakeX, headOfSnakeY);

				case 8:
					return checkIfSnakeIsAliveWhileGoingOnYAxis(snakeBendSize, headOfSnakeX, headOfSnakeY);

				default:
					return true;
				}
			}
			return true;
		}

		private boolean checkIfSnakeIsAliveWhileGoingOnXAxis(int snakeBendSize, int headOfSnakeX, int headOfSnakeY) {
			for (int index = 0; index < snakeBendSize - 2; index++) {
				int xCoordAtIndex = snake.getxCoord().get(index);
				if (headOfSnakeX == xCoordAtIndex) {
					int yCoordAtIndex = snake.getyCoord().get(index);
					int yCoordAtIndexPlusOne = snake.getyCoord().get(index + 1);
					if (isHeadOfSnakeCrossingCoordYAtOnePoint(headOfSnakeY, yCoordAtIndex, yCoordAtIndexPlusOne)) {
						return false;
					}
				}
			}
			return true;
		}

		private boolean isHeadOfSnakeCrossingCoordYAtOnePoint(int headOfSnakeY, int yCoordAtIndex,
				int yCoordAtIndexPlusOne) {
			if (yCoordAtIndex > yCoordAtIndexPlusOne) {
				if (headOfSnakeY < yCoordAtIndex && headOfSnakeY > yCoordAtIndexPlusOne) {
					return true;
				}
			} else {
				if (headOfSnakeY < yCoordAtIndexPlusOne && headOfSnakeY > yCoordAtIndex) {
					return true;
				}
			}
			return false;
		}

		private boolean checkIfSnakeIsAliveWhileGoingOnYAxis(int snakeBendSize, int headOfSnakeX, int headOfSnakeY) {
			for (int index = 0; index < snakeBendSize - 2; index++) {
				int yCoordAtIndex = snake.getyCoord().get(index);
				if (headOfSnakeY == yCoordAtIndex) {
					int xCoordAtIndex = snake.getxCoord().get(index);
					int xCoordAtIndexPlusOne = snake.getxCoord().get(index + 1);
					if (isHeadOfSnakeCrossingCoordXAtOnePoint(headOfSnakeX, xCoordAtIndex, xCoordAtIndexPlusOne)) {
						return false;
					}
				}
			}
			return true;
		}

		private boolean isHeadOfSnakeCrossingCoordXAtOnePoint(int headOfSnakeX, int xCoordAtIndex,
				int xCoordAtIndexPlusOne) {
			if (xCoordAtIndex > xCoordAtIndexPlusOne) {
				if (headOfSnakeX < xCoordAtIndex && headOfSnakeX > xCoordAtIndexPlusOne) {
					return true;
				}
			} else {
				if (headOfSnakeX < xCoordAtIndexPlusOne && headOfSnakeX > xCoordAtIndex) {
					return true;
				}
			}
			return false;
		}
	}

	void controlBounds() {
		switch (snake.getDirection()) {
		case 2:
			if (snake.getyCoord().get(snake.getyCoord().size() - 1) >= Properties.getyBound() - 10) {
				publishSnakeHitBorderEvent();
			}
			break;

		case 4:
			if (snake.getxCoord().get(snake.getxCoord().size() - 1) <= 10) {
				publishSnakeHitBorderEvent();
			}
			break;
		case 6:
			if (snake.getxCoord().get(snake.getxCoord().size() - 1) >= Properties.getxBound() - 10) {
				publishSnakeHitBorderEvent();
			}
			break;
		case 8:
			if (snake.getyCoord().get(snake.getyCoord().size() - 1) <= 10) {
				publishSnakeHitBorderEvent();
			}
			break;

		default:
			break;
		}
	}

	private void publishSnakeHitBorderEvent() {
		ImageIcon imgIcon = new ImageIcon(bufImgIcon);
		playSnakeBoundSound();
		int choice = JOptionPane.showConfirmDialog(null,
				Properties.FAILURE_MESSAGE + Properties.BACKSLASH_N_YOUR_SCORE_IS + Properties.getScore(),
				Properties.FAILURE_TITLE, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, imgIcon);
		Properties.setScore(0);
		if (choice == JOptionPane.YES_NO_OPTION) {
			resetGame();
		} else {
			exitGame();
		}
	}

	private void resetGame() {
		try {
			snake.setDirection(8);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		snake.getxCoord().removeAll(snake.getxCoord());
		snake.getyCoord().removeAll(snake.getyCoord());
		snake.getxCoord().add(Properties.getInitX1());
		snake.getxCoord().add(Properties.getInitX2());
		snake.getyCoord().add(Properties.getInitY1());
		snake.getyCoord().add(Properties.getInitY2());
	}

	private void exitGame() {
		SnakeGUI snakeGUI = SnakeGameContext.getSnakeGui();
		snakeGUI.getMainFrame().remove(snakeGUI.getSnakeGraphicsEngine());
		snakeGUI.setCustomLayout();
		snakeGUI.getMainFrame().repaint();
		snakeGUI.getMainFrame().revalidate();
		freeSubThreads();
		freeResources();
	}

	private class FoodHandler implements ActionListener {

		private void makeRandomFood() {
			Properties.setFoodXCoord(50 + Properties.getRandomInt(Properties.getxBound() - 100));
			Properties.setFoodYCoord(50 + Properties.getRandomInt(Properties.getyBound() - 100));
		}

		private void playSnakeFoodSound() {
			try {
				Properties.getSnakeFood().start();
				Properties.getSnakeFood().addLineListener((LineEvent event) -> {
					if (Type.STOP.equals(event.getType())) {
						Properties.getSnakeFood().setMicrosecondPosition(0);
						Properties.getSnakeFood().setFramePosition(0);
					}
				});

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if (isFoodTimerRunning) {
				if ((snakeX >= Properties.getFoodXCoord() - 1 && snakeX <= Properties.getFoodXCoord() + 11
						&& snakeY >= Properties.getFoodYCoord() - 1 && snakeY <= Properties.getFoodYCoord() + 11)) {
					makeRandomFood();
					Properties.setScore(Properties.getScore() + 50);
					switch (snake.getDirection()) {
					case 2:
						snake.getyCoord().set(snake.getxCoord().size() - 1,
								snake.getyCoord().get(snake.getxCoord().size() - 1) + 10);
						break;
					case 4:
						snake.getxCoord().set(snake.getxCoord().size() - 1,
								snake.getxCoord().get(snake.getxCoord().size() - 1) - 10);
						break;

					case 6:
						snake.getxCoord().set(snake.getxCoord().size() - 1,
								snake.getxCoord().get(snake.getxCoord().size() - 1) + 10);
						break;

					case 8:
						snake.getyCoord().set(snake.getxCoord().size() - 1,
								snake.getyCoord().get(snake.getxCoord().size() - 1) - 10);
						break;

					default:
						break;
					}
					playSnakeFoodSound();
				}
				if (!isEaten) {
					makeRandomFood();
					isEaten = !isEaten;

				}
			}

		}
	}

	private void playSnakeBoundSound() {
		try {
			Properties.getSnakebound().start();
			Properties.getSnakebound().addLineListener((LineEvent event) -> {
				if (Type.STOP.equals(event.getType())) {
					Properties.getSnakebound().setMicrosecondPosition(0);
					Properties.getSnakebound().setFramePosition(0);
				}
			});

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	public SnakeGraphicsEngine getSnakeGraphicsEngine() {
		return graphicsEngine;
	}

	public Snake getSnake() {
		return snake;
	}

}
