package io.github.tolgadurak.game.snake;

import java.io.IOException;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SnakeGame {
	protected static final SnakeGame snakeGame = new SnakeGame();
	private SnakeGUI snakeGui;
	private static final Logger logger = LogManager.getLogger(SnakeGame.class);

	protected SnakeGame() {
		setLookAndFeel();
	}

	protected void initializeSnakeGUI() {
		logger.info("GUI is being initialized..");
		java.awt.EventQueue.invokeLater(() -> {
			try {
				Properties.init();
				SnakeGUI snakeGuiBean = new SnakeGUI();
				snakeGuiBean.getMainFrame().setVisible(true);
				this.snakeGui = snakeGuiBean;

			} catch (IOException e) {
				logger.fatal(e.getMessage(), e);
			}
			logger.info("Program started...");
		});
	}

	protected static SnakeGUI getSnakeGui() {
		if (snakeGame.snakeGui == null) {
			logger.warn("Snake GUI has not been initialized yet, call initializeSnakeGUI first!");
		}
		return snakeGame.snakeGui;
	}

	private void setLookAndFeel() {
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					break;
				}
			}
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			logger.fatal(e.getMessage(), e);
		}
	}

}
