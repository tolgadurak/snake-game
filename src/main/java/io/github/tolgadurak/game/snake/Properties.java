package io.github.tolgadurak.game.snake;

import java.io.IOException;
import java.net.URL;
import java.util.Random;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Properties {
	public static final String FAILURE_MESSAGE = "You failed. Do you wanna retry?";
	public static final String FAILURE_TITLE = "Game Over";
	public static final String APPLICATION_TITLE = "Snake Game v.1.0.0";
	public static final String BACKSLASH_N_YOUR_SCORE_IS = "\nYour score is ";
	private static int xBound;
	private static int yBound;
	private static int foodXCoord;
	private static int foodYCoord;
	private static int speedOfGame = 10;
	private static int selectedDifficulty = 2;
	private static int selectedLength = 0;
	private static int initX1 = 300;
	private static int initX2 = 300;
	private static int initY1 = 300;
	private static int initY2 = 200;
	private static Clip snakeFood;
	private static Clip snakeEaten;
	private static Clip snakebound;
	private static int score;
	private static final Random RANDOM = new Random();
	private static final Logger logger = LogManager.getLogger(Properties.class);

	static {
		try {
			URL defaultSound1 = Properties.class.getResource("sound/snakeFood.wav");
			URL defaultSound2 = Properties.class.getResource("sound/bounds.wav");
			URL defaultSound3 = Properties.class.getResource("sound/eatenSnake.wav");

			AudioInputStream snakeFoodStream = AudioSystem.getAudioInputStream(defaultSound1);
			AudioInputStream snakeBoundsStream = AudioSystem.getAudioInputStream(defaultSound2);
			AudioInputStream snakeEatenStream = AudioSystem.getAudioInputStream(defaultSound3);

			snakeFood = AudioSystem.getClip();
			snakeEaten = AudioSystem.getClip();
			snakebound = AudioSystem.getClip();

			snakeFood.open(snakeFoodStream);
			snakebound.open(snakeBoundsStream);
			snakeEaten.open(snakeEatenStream);

		} catch (LineUnavailableException | UnsupportedAudioFileException | IOException e) {
			logger.error(e.getMessage(), e);
		}
	}

	private Properties() {

	}

	public static void setxBound(int xBound) {
		Properties.xBound = xBound;
	}

	public static void setyBound(int yBound) {
		Properties.yBound = yBound;
	}

	public static void setInitY1(int initY1) {
		Properties.initY1 = initY1;
	}

	public static void setInitY2(int initY2) {
		Properties.initY2 = initY2;
	}

	public static void setSelectedDifficulty(int selectedDifficulty) {
		Properties.selectedDifficulty = selectedDifficulty;
	}

	public static void setSelectedLength(int selectedLength) {
		Properties.selectedLength = selectedLength;
	}

	public static void setSpeedOfGame(int speedOfGame) {
		Properties.speedOfGame = speedOfGame;
	}

	public static void setFoodXCoord(int value) {
		foodXCoord = value;
	}

	public static void setFoodYCoord(int value) {
		foodYCoord = value;
	}

	public static int getRandomInt(int bound) {
		return RANDOM.nextInt(bound);
	}

	public static void init() {
		// Initializes static properties
	}

	public static Clip getSnakeFood() {
		return snakeFood;
	}

	public static void setSnakeFood(Clip snakeFood) {
		Properties.snakeFood = snakeFood;
	}

	public static Clip getSnakeEaten() {
		return snakeEaten;
	}

	public static void setSnakeEaten(Clip snakeEaten) {
		Properties.snakeEaten = snakeEaten;
	}

	public static Clip getSnakebound() {
		return snakebound;
	}

	public static void setSnakebound(Clip snakebound) {
		Properties.snakebound = snakebound;
	}

	public static int getScore() {
		return score;
	}

	public static void setScore(int score) {
		Properties.score = score;
	}

	public static int getxBound() {
		return xBound;
	}

	public static int getyBound() {
		return yBound;
	}

	public static int getFoodXCoord() {
		return foodXCoord;
	}

	public static int getFoodYCoord() {
		return foodYCoord;
	}

	public static int getSpeedOfGame() {
		return speedOfGame;
	}

	public static int getSelectedDifficulty() {
		return selectedDifficulty;
	}

	public static int getSelectedLength() {
		return selectedLength;
	}

	public static int getInitX1() {
		return initX1;
	}

	public static int getInitX2() {
		return initX2;
	}

	public static int getInitY1() {
		return initY1;
	}

	public static int getInitY2() {
		return initY2;
	}

}
