package io.github.tolgadurak.game.snake;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.GeneralPath;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.Timer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SnakeGraphicsEngine extends JPanel {
	private Timer mainTimer;
	private int emptyBorder = 5;
	private static final long serialVersionUID = 1L;
	private transient Snake snake;
	private transient BufferedImage bufImg;
	private static final Logger logger = LogManager.getLogger(SnakeGraphicsEngine.class);

	public SnakeGraphicsEngine(final Snake snake) throws IOException {
		InputStream is = getClass().getResourceAsStream("image/game_play.jpg");
		try {
			bufImg = ImageIO.read(is);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		this.snake = snake;
		repaint();
		mainTimer = new Timer(Properties.getSpeedOfGame(), new CoreGraphics());

	}

	private class CoreGraphics implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			repaint();
		}
	}

	public void freeMainTimer() {
		mainTimer.stop();
	}

	@Override
	public void paintComponent(java.awt.Graphics e) {
		Properties.setxBound(getSize().width);
		Properties.setyBound(getSize().height);
		Graphics2D g2 = (Graphics2D) e;
		g2.setStroke(new BasicStroke(10.0f));
		g2.drawImage(bufImg, 0, 0, Properties.getxBound(), Properties.getyBound(),
				(Image img, int infoflags, int x, int y, int width, int height) -> false);
		e.setColor(Color.WHITE);
		g2.fillRect(Properties.getFoodXCoord(), Properties.getFoodYCoord(), 10, 10);
		g2.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 15));
		g2.drawString("Your score: " + Properties.getScore(), Properties.getxBound() - 140,
				Properties.getyBound() - 25);
		e.setColor(Color.BLACK);
		g2.drawRect(emptyBorder, emptyBorder, Properties.getxBound() - 2 * emptyBorder,
				Properties.getyBound() - 2 * emptyBorder);
		g2.setStroke(new BasicStroke(10.0f));
		GeneralPath path = new GeneralPath(GeneralPath.WIND_EVEN_ODD, snake.getxCoord().size());
		path.moveTo(snake.getxCoord().get(0), snake.getyCoord().get(0));
		for (int i = 0; i < snake.getxCoord().size(); i++) {
			path.lineTo(snake.getxCoord().get(i), snake.getyCoord().get(i));
		}
		e.setColor(Color.WHITE);
		g2.draw(path);
	}

	public Timer getMainTimer() {
		return mainTimer;
	}
}
