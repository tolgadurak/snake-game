package io.github.tolgadurak.game.snake;

import java.awt.event.ItemEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

public class SnakeSettings {

	private javax.swing.JFrame frame = new javax.swing.JFrame();

	public SnakeSettings() throws IOException {
		difficulties = new String[] { "Kids Mode", "Novice", "Experienced", "Very Hard", "Impossible" };
		snakeLength = new String[] { "Short", "Medium", "Long", "Extra Long" };
		initComponents();
		setProgramIcon();
		selectedLength = Properties.getSelectedLength();
		selectedDifficulty = Properties.getSelectedDifficulty();
		initY1 = Properties.getInitY1();
		initY2 = Properties.getInitY2();
	}

	public javax.swing.JFrame getFrame() {
		return frame;
	}

	public javax.swing.JButton getSaveButton() {
		return saveButton;
	}

	private void setProgramIcon() throws IOException {
		InputStream is = getClass().getResourceAsStream("image/snake-icon.png");
		BufferedImage bufImg = ImageIO.read(is);
		frame.setIconImage(bufImg);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void initComponents() {

		java.awt.Font fontTahoma114 = new java.awt.Font("Tahoma", 1, 14);
		java.awt.Font fontTahoma111 = new java.awt.Font("Tahoma", 1, 11);
		java.awt.Font fontSketchFlowPrint130 = new java.awt.Font("SketchFlow Print", 1, 30);
		javax.swing.JPanel settingsPanel = new javax.swing.JPanel();
		javax.swing.JLabel difficultyLabel = new javax.swing.JLabel();
		javax.swing.JLabel lengthOfSnakeLabel = new javax.swing.JLabel();
		javax.swing.JCheckBox musicRadioButton = new javax.swing.JCheckBox();
		javax.swing.JCheckBox soundRadioButton = new javax.swing.JCheckBox();
		difficultyComboBox = new javax.swing.JComboBox();
		lengthOfSnakeComboBox = new javax.swing.JComboBox();
		javax.swing.JPanel labelPanel = new javax.swing.JPanel();
		javax.swing.JLabel snakeSettingsLabel = new javax.swing.JLabel();
		javax.swing.JPanel buttonsPanel = new javax.swing.JPanel();
		saveButton = new javax.swing.JButton();
		javax.swing.JButton cancelButton = new javax.swing.JButton();
		saveButton.setEnabled(false);

		frame.setTitle("Game Settings");
		frame.setBounds(new java.awt.Rectangle(0, 0, 0, 0));
		frame.setResizable(false);

		difficultyLabel.setFont(fontTahoma114);
		difficultyLabel.setText("Difficulty");

		lengthOfSnakeLabel.setFont(fontTahoma114);
		lengthOfSnakeLabel.setText("Length of snake");

		musicRadioButton.setFont(fontTahoma114);
		musicRadioButton.setText("Music");

		soundRadioButton.setFont(fontTahoma114);
		soundRadioButton.setText("Sound");

		difficultyComboBox.setModel(new javax.swing.DefaultComboBoxModel(difficulties));
		difficultyComboBox.addItemListener(this::difficultyComboBoxItemStateChanged);
		lengthOfSnakeComboBox.setModel(new javax.swing.DefaultComboBoxModel(snakeLength));
		lengthOfSnakeComboBox.addItemListener(this::lengthOfSnakeComboBoxItemStateChanged);
		difficultyComboBox.setSelectedIndex(Properties.getSelectedDifficulty());
		lengthOfSnakeComboBox.setSelectedIndex(Properties.getSelectedLength());

		javax.swing.GroupLayout settingsPanelLayout = new javax.swing.GroupLayout(settingsPanel);
		settingsPanel.setLayout(settingsPanelLayout);
		settingsPanelLayout.setHorizontalGroup(settingsPanelLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(settingsPanelLayout.createSequentialGroup().addContainerGap().addGroup(settingsPanelLayout
						.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(settingsPanelLayout.createSequentialGroup().addComponent(lengthOfSnakeLabel)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(lengthOfSnakeComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE))
						.addGroup(settingsPanelLayout.createSequentialGroup()
								.addComponent(difficultyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 111,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(
										difficultyComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(settingsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(musicRadioButton, javax.swing.GroupLayout.Alignment.TRAILING,
										javax.swing.GroupLayout.PREFERRED_SIZE, 71,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(soundRadioButton, javax.swing.GroupLayout.Alignment.TRAILING))
						.addContainerGap()));
		settingsPanelLayout.setVerticalGroup(settingsPanelLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(settingsPanelLayout.createSequentialGroup().addContainerGap(11, Short.MAX_VALUE)
						.addGroup(settingsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(difficultyLabel).addComponent(musicRadioButton)
								.addComponent(difficultyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(settingsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(lengthOfSnakeLabel).addComponent(soundRadioButton)
								.addComponent(lengthOfSnakeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.PREFERRED_SIZE))));

		snakeSettingsLabel.setFont(fontSketchFlowPrint130);
		snakeSettingsLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		snakeSettingsLabel.setText("Snake Settings ");

		javax.swing.GroupLayout labelPanelLayout = new javax.swing.GroupLayout(labelPanel);
		labelPanel.setLayout(labelPanelLayout);
		labelPanelLayout
				.setHorizontalGroup(labelPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
								labelPanelLayout.createSequentialGroup()
										.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(snakeSettingsLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 290,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addContainerGap()));
		labelPanelLayout
				.setVerticalGroup(labelPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
								labelPanelLayout.createSequentialGroup()
										.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(snakeSettingsLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 51,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addContainerGap()));

		saveButton.setFont(fontTahoma111);
		saveButton.setText("Save");
		saveButton.addActionListener((java.awt.event.ActionEvent evt) -> saveButtonActionPerformed());
		cancelButton.setFont(fontTahoma111);
		cancelButton.setText("Cancel");
		cancelButton.addActionListener((java.awt.event.ActionEvent evt) -> cancelButtonActionPerformed());
		javax.swing.GroupLayout buttonsPanelLayout = new javax.swing.GroupLayout(buttonsPanel);
		buttonsPanel.setLayout(buttonsPanelLayout);
		buttonsPanelLayout
				.setHorizontalGroup(buttonsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(buttonsPanelLayout.createSequentialGroup().addContainerGap(90, Short.MAX_VALUE)
								.addComponent(saveButton).addGap(30, 30, 30).addComponent(cancelButton)
								.addGap(65, 65, 65)));
		buttonsPanelLayout.setVerticalGroup(buttonsPanelLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(buttonsPanelLayout.createSequentialGroup().addContainerGap()
						.addGroup(buttonsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(saveButton).addComponent(cancelButton))
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(frame.getContentPane());
		frame.getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(buttonsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
						Short.MAX_VALUE)
				.addGroup(layout.createSequentialGroup()
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
								.addGroup(javax.swing.GroupLayout.Alignment.LEADING,
										layout.createSequentialGroup().addContainerGap().addComponent(settingsPanel,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.addComponent(labelPanel, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(0, 0, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addComponent(labelPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 58,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(settingsPanel, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(buttonsPanel,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE)));
		soundRadioButton.setEnabled(false);
		musicRadioButton.setEnabled(false);
		frame.pack();
	}// </editor-fold>

	private void cancelButtonActionPerformed() {
		frame.setVisible(false);
	}

	private void saveButtonActionPerformed() {
		Properties.setSpeedOfGame(speedOfGame);
		Properties.setSelectedDifficulty(selectedDifficulty);
		Properties.setSelectedLength(selectedLength);
		Properties.setInitY1(initY1);
		Properties.setInitY2(initY2);
		frame.setVisible(false);
	}

	private void difficultyComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {
		if (evt.getStateChange() == ItemEvent.SELECTED) {
			if (evt.getItem().equals(difficulties[0])) {
				changeGameDifficulty(35, 0);
			} else if (evt.getItem().equals(difficulties[1])) {
				changeGameDifficulty(20, 1);
			} else if (evt.getItem().equals(difficulties[2])) {
				changeGameDifficulty(10, 2);
			} else if (evt.getItem().equals(difficulties[3])) {
				changeGameDifficulty(5, 3);
			} else if (evt.getItem().equals(difficulties[4])) {
				changeGameDifficulty(1, 4);
			}
		}

	}

	private void changeGameDifficulty(int speedOfGame, int selectedDifficulty) {
		this.speedOfGame = speedOfGame;
		this.selectedDifficulty = selectedDifficulty;
		if (Properties.getSelectedDifficulty() != selectedDifficulty) {
			if (!saveButton.isEnabled())
				saveButton.setEnabled(true);
		} else if (Properties.getSelectedDifficulty() == selectedDifficulty
				&& Properties.getSelectedLength() == selectedLength) {
			saveButton.setEnabled(false);
		}
	}

	private void lengthOfSnakeComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {
		if (evt.getStateChange() == ItemEvent.SELECTED) {
			saveButton.setEnabled(true);
			if (evt.getItem().equals(snakeLength[0])) {
				changeLengthOfSnake(0, 300, 200);
			} else if (evt.getItem().equals(snakeLength[1])) {
				changeLengthOfSnake(1, 400, 200);
			} else if (evt.getItem().equals(snakeLength[2])) {
				changeLengthOfSnake(2, 450, 200);
			} else if (evt.getItem().equals(snakeLength[3])) {
				changeLengthOfSnake(3, 500, 200);
			}
		}
	}

	private void changeLengthOfSnake(int selectedLength, int initY1, int initY2) {
		this.selectedLength = selectedLength;
		this.initY1 = initY1;
		this.initY2 = initY2;
		if (Properties.getSelectedLength() != selectedLength) {
			if (!saveButton.isEnabled())
				saveButton.setEnabled(true);
		} else if (Properties.getSelectedLength() == selectedLength
				&& Properties.getSelectedDifficulty() == selectedDifficulty) {
			saveButton.setEnabled(false);
		}
	}

	@SuppressWarnings("rawtypes")
	private javax.swing.JComboBox difficultyComboBox;
	@SuppressWarnings("rawtypes")
	private javax.swing.JComboBox lengthOfSnakeComboBox;
	private javax.swing.JButton saveButton;
	private String[] difficulties;
	private String[] snakeLength;
	private int speedOfGame;
	private int selectedDifficulty;
	private int selectedLength;
	private int initY1;
	private int initY2;

}
