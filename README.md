# Snake Game 1.0.0

## A simple snake game written entirely in Java language.

### Native macOS bundler script:
javapackager -deploy -native dmg -name 'Snake Game' -Bicon=SnakeGame.icns -BappVersion=1.0.0 -srcdir . -srcfiles snake-game-1.0.0.jar -appclass io.github.tolgadurak.game.snake.SnakeGameContext -outdir out -outfile snake-game -v